Le soir tombait ; la lutte était ardente et noire.
Il avait l’offensive et presque la victoire ;
Il tenait Wellington acculé sur un bois.
Sa lunette à la main, il observait parfois
Le centre du combat, point obscur où tressaille
La mêlée, effroyable et vivante broussaille,
Et parfois l’horizon, sombre comme la mer.
Soudain, joyeux, il dit : Grouchy ! – C’était Blücher.
L’espoir changea de camp, le combat changea d’âme,
La mêlée en hurlant grandit comme une flamme.
La batterie anglaise écrasa nos carrés.
La plaine, où frissonnaient les drapeaux déchirés,
Ne fut plus, dans les cris des mourants qu’on égorge,
Qu’un gouffre flamboyant, rouge comme une forge ;
Gouffre où les régiments comme des pans de murs
Tombaient, où se couchaient comme des épis mûrs
Les hauts tambours-majors aux panaches énormes,
Où l’on entrevoyait des blessures difformes !
Carnage affreux ! moment fatal ! L’homme inquiet
Sentit que la bataille entre ses mains pliait.